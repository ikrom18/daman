<?php

class WoPsb extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $no_sc;

    /**
     *
     * @var string
     */
    public $no_voice;

    /**
     *
     * @var string
     */
    public $no_internet;

    /**
     *
     * @var string
     */
    public $odp_wo;

    /**
     *
     * @var string
     */
    public $odp_real;

    /**
     *
     * @var integer
     */
    public $odp_port;

    /**
     *
     * @var string
     */
    public $sn_ont;

    /**
     *
     * @var string
     */
    public $date;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("daman");
        $this->setSource("wo_psb");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'wo_psb';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return WoPsb[]|WoPsb|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return WoPsb|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
